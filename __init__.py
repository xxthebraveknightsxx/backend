from flask import Flask
from models import db
import models
import views

from flask_cors import CORS


def create_app():
    app = Flask(__name__)
    CORS(app)
    models.init_app(app)
    db.init_app(app)
    views.init_app(app)
    return app


app = create_app()
