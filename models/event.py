from app.models import Team
from .base import db

teams = db.Table('teams',
                 db.Column('team_id', db.Integer, db.ForeignKey('team.id'), primary_key=True),
                 db.Column('event_id', db.Integer, db.ForeignKey('event.id'), primary_key=True))


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # date = db.Column(db.DATE, nullable=False) **** use a date object when getting params from ofir
    date = db.Column(db.Integer, nullable=False)
    # orders = db.relationship('Order', backref='bar', lazy=True)
    teams = db.relationship('Team', secondary=teams, backref=db.backref('events', lazy=True),
                            lazy='subquery')
    bar_event_info = db.relationship('BarEventInfo', backref='event', lazy=True)

    @classmethod
    def create(cls, date, team1_id, team2_id):
        print("in create event method")
        team1 = Team.query.filter_by(id=team1_id).first()
        team2 = Team.query.filter_by(id=team2_id).first()

        event = cls(date=date)
        event.teams.append(team1)
        event.teams.append(team2)
        try:
            db.session.add(event)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise

        return event

    def __repr__(self):
        return "id is {id}, team1 is {team1} team2 is " \
               "{team2}".format(id=self.id, team1=self.teams[0], team2=self.teams[1])
