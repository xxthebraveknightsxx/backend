from .base import db


class BarEventInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    place = db.Column(db.Integer)
    # orders = db.relationship('Order', backref='order', lazy=True)
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'), nullable=False)
    bar_id = db.Column(db.Integer, db.ForeignKey('bar.id'), nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'), nullable=False)
    orders = db.relationship('Order', backref='bar_event_info', lazy=True)

    def __repr__(self):
        string = "id is {id}, event_id is {event_id} bar_id is {bar_id} team_id is " \
                 "{team_id}".format(id=self.id, event_id=self.event_id, bar_id=self.bar_id,
                                    team_id=self.team_id)
        for order in self.orders:
            string += order.__repr__()
        return string
