from .base import db
from bar import Bar
from bar_event_info import BarEventInfo
from user import User
from team import Team
from order import Order
from event import Event


def init_app(app):
    db.init_app(app)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test5.db'


__all__ = [Bar, User, Team, Order, Event, BarEventInfo, db, init_app]
