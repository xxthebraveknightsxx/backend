from .base import db


class Bar(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    address = db.Column(db.String(120), unique=True, nullable=False)
    picture_path = db.Column(db.String(120), unique=True, nullable=False)
    # orders = db.relationship('Order', backref='bar', lazy=True)
    events = db.relationship('BarEventInfo', backref='bar_event', lazy=True)

    @classmethod
    def create(cls, name, address, picture_path):
        print("in create bar method")
        bar = cls(name=name, address=address, picture_path=picture_path)
        try:
            db.session.add(bar)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise

        return bar

    def __repr__(self):
        return "id is {id}, bar name is {name} address is " \
               "{address}".format(id=self.id, name=self.name, address=self.address)



