from .base import db


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    country = db.Column(db.String(120), nullable=False)
    picture_path = db.Column(db.String(120), unique=True, nullable=False)

    @classmethod
    def create(cls, name, country, picture_path):
        print('in team create method')
        team = cls(name=name, country=country, picture_path=picture_path)
        try:
            db.session.add(team)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise

        return team

    def __repr__(self):
        return "id is {id}, team name is {name} country is " \
               "{country}".format(id=self.id, name=self.name, address=self.country)
