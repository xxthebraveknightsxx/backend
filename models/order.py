from .base import db


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    bar_event_info_id = db.Column(db.Integer, db.ForeignKey('bar_event_info.id'), nullable=False)

    def __repr__(self):
        return "id is {id}, user_id is {user_id} bar_id is " \
               "{bar_event_info_id}".format(id=self.id, user_id=self.user_id,
                                            bar_event_info_id=self.bar_event_info.id)
