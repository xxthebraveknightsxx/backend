from .base import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    # orders = db.relationship('Order', backref='user', lazy=True)

    def __repr__(self):
        return "id is {id}, username is {username} email is {email}".format(id=self.id,
                                                                            username=self.username, email=self.email)
