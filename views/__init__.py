from .user import bp as user_bp
from .team import bp as team_bp
from .bar import bp as bar_bp
from .event import bp as event_bp
from .order import bp as order_bp
from .bar_event_info import bp as event_bar_info_bp


def init_app(app):
    print('in views init_app')
    app.register_blueprint(user_bp)
    app.register_blueprint(team_bp)
    app.register_blueprint(bar_bp)
    app.register_blueprint(event_bp)
    app.register_blueprint(order_bp)
    app.register_blueprint(event_bar_info_bp)


__all__ = [init_app]
