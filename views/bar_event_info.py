from ..models import BarEventInfo, Bar, Event, Team, db
from flask import Blueprint

bp = Blueprint('event_info', __name__, url_prefix='/event_info')


@bp.route('/add_bar_event/<event_id>')
def add_bar_event(event_id, bar_id=1, team_id=1, place=12):
    # check that there is no other event in this bar at that time.
    new_event = Event.query.filter_by(id=event_id).first()
    for event in Event.query.filter_by(bar_id=bar_id):
        if event.date == new_event.date: # change this to DATE stuff.
            return 'there is already an event schedule for that time in that bar.'

    # getting this info from the db to make sure it exists
    event = Event.query.filter_by(id=event_id).first()
    bar = Bar.query.filter_by(id=bar_id).first()
    team = Team.query.filter_by(id=team_id).first()
    bar_event_info = BarEventInfo(event_id=event.id, place=place, team_id=team.id, bar_id=bar.id)
    db.add(bar_event_info)
    db.commit()
    return 'there is a new bar_event_info ', bar_event_info.__repr__()


@bp.route('/<id>')
def get_event_info(id):
    event = Event.query.filter_by(id=id).first()
    return event.__repr__()


@bp.route('/delete/<id>')
def delete_bar_event(id):
    bar_event = BarEventInfo.query.filter_by(id=id).first()
    db.session.delete(bar_event)
    db.session.commit()




