from ..models import Event, BarEventInfo, Bar, Team, db
from flask import Blueprint

bp = Blueprint('event', __name__, url_prefix='/event')


@bp.route('/<date>')
def add_event(date, team1_id=1, team2_id=2):
    event = Event.create(date=date, team1_id=team1_id, team2_id=team2_id)
    try:
        db.session.add(event)
        db.session.commit()
    except Exception:
        db.session.rollback()
        raise
    return 'added an event.'


@bp.route('/delete/<id>')
def delete_event(id):
    event = Event.query.filter_by(id=id).first()
    db.session.delete(event)
    db.session.commit()


