from ..models import User, BarEventInfo, db, Order
from flask import Blueprint

bp = Blueprint('user', __name__, url_prefix='/user')


@bp.route('/<username>', methods=['GET', 'POST'])
def add_user(username):
    print 'fd'
    user = User(username=username, email='fhjkjd@gmail')
    try:
        db.create_all()
        db.session.add(user)
        db.session.commit()
        print('added user to the db')
    except Exception:
        db.session.rollback()
        raise

    #
    #
    # #TODO: use a render function to return html response with user
    return 'added user'


@bp.route('/delete/<id>')
def delete_user(id):
    try:
        user = User.query.filter_by(id=id).first()
        db.session.delete(user)
        db.session.commit()
        return 'deleted user ', user.id
    except Exception as e:
        return 'could not delete user. exception: ', e.message