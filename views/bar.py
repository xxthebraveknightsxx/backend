from ..models import Bar, db
from flask import Blueprint, request

bp = Blueprint('bar', __name__, url_prefix='/bar')


@bp.route('/<name>', methods=['GET', 'POST'])
def add_bar(name):
    print('fdgd')
    print('the json is ', request.view_args)
    bar = Bar.create(name=name, address='fddssfdfdddffddddd', picture_path='/hd/ddfdddddgd')
    try:
        db.create_all()
        db.session.add(bar)
        db.session.commit()
        return 'added a bar. name is {name}, id is {id}'.format(name=bar.name, id=bar.id)
    except Exception:
        db.session.rollback()
        raise


@bp.route('/delete/<id>')
def delete_bar(id):
    bar = Bar.query.filter_by(id=id).first()
    db.session.delete(bar)
    db.session.commit()
