from ..models import Team, db
from flask import Blueprint

bp = Blueprint('team', __name__, url_prefix='/team')


@bp.route('/<name>')
def add_team(name):
    print('fdgd')
    team = Team.create(name=name, country='ir', picture_path='/home/usffer/pidc.jpg')
    try:
        db.session.add(team)
        db.session.commit()
    except Exception:
        db.session.rollback()
        raise
    return 'added a team.'


@bp.route('/delete/<id>')
def delete_team(id):
    try:
        team = Team.query.filter_by(id=id).first()
        db.session.delete(team)
        db.session.commit()
        return 'deleted team ', team.id
    except Exception as e:
        return 'could not delete team. exception: ', e.message