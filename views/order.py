from ..models import User, BarEventInfo, Order, db
from flask import Blueprint


bp = Blueprint('order', __name__, url_prefix='/orders')


@bp.route('/add_order/<quantity>')
def add_order(quantity, bar_info_id=1, user_id=1):
    # user = User(username='mfdfddifdc', email='ffdddgfddddddfd@gmail')
    bar_event_info = BarEventInfo.query.filter_by(id=bar_info_id).first()
    user = User.query.filter_by(id=user_id).first()
    order = Order(quantity=quantity, user_id=user.id, bar_event_info_id=bar_event_info.id)
    try:
        db.create_all()
        db.session.add(order)
        db.session.commit()
        print('added order to the db')
    except Exception:
        db.session.rollback()
        raise
    return 'added user order'


@bp.route('/delete_order/<id>')
def delete_order(id):
    order = Order.query.filter_by(id=id).first()
    db.session.delete(order)
    db.session.commit()
